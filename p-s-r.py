#--coding:utf-8

from random import randint  #импорт генератора случайных чисел
from time import sleep      #импорт функции задержки

game = True

values = ['p', 's', 'r']
while(game):
    choice = raw_input(u'paper(p), scissors(s) or rock(r)')

    rnd = randint(0, 2)

    choice2 = values[rnd]

    result = choice + choice2

    print u'Ждем ход противника'
    sleep(2) #ждем две секунды
    print(u'Результат раунда  ' + choice + u' vs ' + choice2)

    if choice == choice2:
        print u'Победила дружба'
    elif result == 'pr' or result == 'rs' or result == 'sp':
        print u'Пользователь победил компьютер!'
    else:
        print u'Восстание машин! Компьютер выиграл!'

    play = raw_input(u'One more time Y/n\n')
    if play == 'n':
        game = False
